const defaultFontSize = 24;

function hideTopBanner(){
    document.querySelector("html body table tbody tr:nth-of-type(1)").hidden = true;
}

function hideLeftMenu(){
    document.querySelector("html body table tbody tr:nth-of-type(2) td:nth-of-type(1)").hidden = true;
}

function hideTopAdvertising(){
    document.querySelector("html body table tbody tr:nth-of-type(2) td:nth-of-type(2) noindex:nth-of-type(1)").hidden = true;
}

function hideBottomBanner(){
    document.querySelector("html body table tbody tr:nth-of-type(2) td:nth-of-type(2) noindex:nth-of-type(2)").hidden = true;
}

function removeBordersAndMargins(){
    document.querySelector("body").style.margin = 0;
	document.querySelector("html body table").style.width = "inherit";
	document.querySelector("html body table").style.border = 0;
}

function resizeScrolls(){
     document.querySelectorAll("html body table tbody tr:nth-of-type(2) td:nth-of-type(2) div").forEach((el) => {
		el.style.height = el.scrollHeight + 150;
		el.style["margin-left"] = 'none';
		el.style["border"] = 'none';
		el.style.overflow = 'hidden';
		el.style.width = 'inherit';
	});
}

function incFontSizeForSelectors(selectors, newFontSize){

    selectors.forEach( (selector) => {
        incFontSizeForSelector(selector, newFontSize);
    });
}

function incFontSizeForSelector(selector, newFontSize){

    var elements = document.querySelectorAll(selector);
    elements.forEach( (el) => {
        el.style["font-size"] = newFontSize;
    });
}


(function() {

    hideTopBanner();
    hideLeftMenu();
    hideTopAdvertising();
    hideBottomBanner();

    resizeScrolls();
    removeBordersAndMargins();

    var selectors = ["p", "a", "strong"];
    incFontSizeForSelectors(selectors, defaultFontSize);
})()