#!/usr/bin/env python3

from os import path
import logging

from scrapper.settings import get_settings
from scrapper.logs import get_logger
from scrapper.page.locator import get_page_locator
from scrapper.page.transformer import get_page_transformer
from scrapper.page.saver import get_page_saver
from scrapper.page.converter import get_page_converter
from scrapper.webDriver import get_driver
from scrapper.utils import wait_random_timeout


class Scrapper(object):

    def __init__(self):
        self.settings = get_settings()
        self.log = get_logger(self.settings.log_name, log_level=logging.INFO)

        self.page_locator = get_page_locator(self.log, self.settings)
        self.page_transformer = get_page_transformer(self.log, self.settings)
        self.page_saver = get_page_saver(self.log, self.settings)
        self.page_converter = get_page_converter(self.log, self.settings)

    def run(self):
        with get_driver(self.log, self.settings) as driver:
            links = self.page_locator.get_target_links(driver)
            self.log.info("Got %s links to process...", len(links))

            for ind, link in enumerate(links):
                self.log.info("Processing link %s, %s from %s", link, ind + 1, len(links))
                try:
                    driver.get(link)
                    wait_random_timeout(2, 4)

                    self.transform_current_page(driver)

                    page_name = driver.get_page_name()
                    self.save_current_page(page_name, driver)
                    self.convert_page(page_name)

                except:
                    self.log.exception("Error save page: {}".format(link))

    def transform_current_page(self, driver):
        self.page_transformer.transform_current_page(driver)
        wait_random_timeout(1, 2)

    def save_current_page(self, page_name, driver):
        self.page_saver.save_current_page(page_name, driver)
        wait_random_timeout(1, 2)

    def convert_page(self, page_name):
        src_fpath = path.join(self.page_saver.get_save_path(), page_name)
        src_fpath = "file://{}.html".format(src_fpath)

        self.page_converter.convert_page(src_fpath, self.settings.store_path)



if __name__ == "__main__":
    Scrapper().run()