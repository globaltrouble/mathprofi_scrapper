import time
import random


def wait_random_timeout(mn, mx):
    time.sleep(get_random_timeout(mn, mx))


def get_random_timeout(mn, mx):
    return random.random() * abs(mx - mn) + mn