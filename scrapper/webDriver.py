import psutil
from abc import abstractmethod
from os import path


def get_driver(log, settings):
    return ChromeDriver(log, settings)


class AbstractDriver(object):

    def __init__(self, log, settings):
        self.log = log
        self.settings = settings
        self.driver = None

    def __enter__(self):
        self._init_driver()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.driver.close()

    @abstractmethod
    def _init_driver(self):
        """
        should initiate self.driver
            self.driver = WebDriver()
        """

    @abstractmethod
    def get(self, url):
        """
        Open target url using selenium webdriver
        :param url: target url, "http://mathprofi.com"
        """

    @abstractmethod
    def execute_script(self, script):
        """
        execute script on the current page
        :param script: js code, "document.write('Hello world!')"
        :return: result of the operation
        """

    @abstractmethod
    def get_window_pid(self):
        """
        :return: process id of the opened window using webdriver
        """

    @abstractmethod
    def get_pid(self):
        """
        :return: process id of the webdriver
        """

    def find_elements(self, by, value):
        return self.driver.find_elements(by, value)

    def get_page_name(self):
        url = path.basename(self.driver.current_url)
        page_name = path.splitext(url)[0]
        return page_name


class ChromeDriver(AbstractDriver):
    def _init_driver(self):
        from selenium.webdriver import Chrome
        self.driver = Chrome()

    def get(self, url):
        self.driver.get(url)

    def execute_script(self, script):
        return self.driver.execute_script(script)

    def get_window_pid(self):
        driver_pid = self.get_pid()

        p = psutil.Process(driver_pid)
        children = p.children()
        window_pid = children[0].pid

        return window_pid

    def get_pid(self):
        return self.driver.service.process.pid