import subprocess
import shlex
from abc import abstractmethod


def get_executor(log, settings):
    return ShellExecutor(log, settings)


class AbstractExecutor(object):

    def __init__(self, log, settings):
        self.log = log
        self.settings = settings

    @abstractmethod
    def exec_cmd(self, cmd):
        pass


class CmdResult(object):
    def __init__(self, stdout, stderr, returncode):
        self.stdout = stdout
        self.stderr = stderr
        self.returncode = returncode


class ShellExecutor(AbstractExecutor):

    def exec_cmd(self, cmd):
        cmd = shlex.split(cmd)

        self.log.debug("Executing cmd: `%s`", cmd)
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = proc.communicate()
        self.log.debug("Done cmd `%s`", cmd)

        return CmdResult(stdout, stderr, proc.returncode)