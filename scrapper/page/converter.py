import os
from abc import abstractmethod
from scrapper.cmdExecutor import get_executor


def get_page_converter(log, settings):
    return PdfPageConverter(log, settings)


class AbstractPageConverter(object):

    def __init__(self, log, settings):
        self.log = log
        self.settings = settings
        self.cmd_executor = get_executor(log, settings)

    @abstractmethod
    def convert_page(self, src_fpath, target_folder):
        """
        Should convert page from src_fpath, and save it to a specific destination
        :param src_fpath: full path to source page
        :param target_folder: folder to save converted pages
        """

    def get_target_name(self, src_path):
        dst_path = "{}.{}".format(self.extract_basename(src_path), self.get_file_extension())
        return dst_path

    @staticmethod
    def extract_basename(page_name):
        extensions = os.path.splitext(os.path.basename(page_name))
        return extensions[0]

    @abstractmethod
    def get_file_extension(self):
        """
        :return: target file extension, example "pdf"
        """


class PdfPageConverter(AbstractPageConverter):

    def convert_page(self, src_fpath, target_folder):

        try:
            fname = self.get_target_name(src_fpath)
            dst_fpath = os.path.join(target_folder, fname)

            self.log.info("Converting page %s to %s", src_fpath, dst_fpath)

            self._check_and_remove_dst_path(dst_fpath)

            cmd = "wkhtmltopdf " \
                  "--page-height 3in " \
                  "--margin-top 0 " \
                  "--margin-bottom 0 " \
                  "--margin-left 0 " \
                  "--margin-right 0 " \
                  "{} {}".format(src_fpath, dst_fpath)

            self.cmd_executor.exec_cmd(cmd)
        except:
            self.log.exception("Error convert page %s", src_fpath)

    def get_file_extension(self):
        return "pdf"

    def _check_and_remove_dst_path(self, dst_path):
        if os.path.exists(dst_path):
            self.log.warning("Warning path already exists, removing {}!".format(dst_path))
            os.remove(dst_path)