from abc import abstractmethod

from scrapper.utils import wait_random_timeout


def get_page_locator(log, settings):
    return SimplePageLocator(log, settings)


class AbstractPageLocator(object):
    def __init__(self, log, settings):
        self.log = log
        self.settings = settings

    @abstractmethod
    def get_target_links(self, driver):
        pass


class SimplePageLocator(AbstractPageLocator):

    def get_target_links(self, driver):

        driver.get(self.settings.start_page)
        wait_random_timeout(2, 4)

        from selenium.webdriver.common.by import By

        tbody = driver.find_elements(By.CSS_SELECTOR, "html body table tbody")

        main_content = tbody[0]
        left_menu = main_content.find_elements(By.CSS_SELECTOR, "td")[1]
        targets = left_menu.find_elements(By.CSS_SELECTOR, "div p a")

        res = list(map(lambda t: t.get_property(name="href"), targets))
        return res