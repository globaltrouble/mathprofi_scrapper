from abc import abstractmethod
from scrapper.cmdExecutor import get_executor

from scrapper.utils import wait_random_timeout


def get_page_saver(log, settings):
    return XdotoolPageSaver(log, settings)


class AbstractPageSaver(object):

    def __init__(self, log, settings):
        self.log = log
        self.settings = settings

    @abstractmethod
    def save_current_page(self, page_name, driver):
        """
        save current page opened in the driver with name page_name
        :param page_name: target name
        :param driver: webdriver
        """

    @abstractmethod
    def get_save_path(self):
        """
        :return:
        """


class XdotoolPageSaver(AbstractPageSaver):
    
    def __init__(self, log, settings):
        super(XdotoolPageSaver, self).__init__(log, settings)
        self.executor = get_executor(log, settings)

    def save_current_page(self, page_name, driver):

        try:
            window_pid = driver.get_window_pid()
            self.log.debug("Window pid %s", window_pid)

            window_id = self._get_window_id_by_pid(window_pid)
            self.log.debug("Window id %s", window_id)

            self._activate_driver_window(window_id)
            self._save_page(page_name)
        except:
            self.log.exception("Error save page %s", page_name)

    def get_save_path(self):
        return self.settings.save_path

    def _get_window_id_by_pid(self, window_pid):
        cmd = "xdotool search --pid {}".format(window_pid)
        stdout = self.executor.exec_cmd(cmd).stdout
        window_id = stdout.split()[-1]

        return window_id

    def _activate_driver_window(self, window_id):

        self.log.debug("Activating window id %s", window_id)
        cmd = "xdotool windowactivate {}".format(int(window_id))
        self.executor.exec_cmd(cmd)

    def _save_page(self, page_name):

        self.log.info("Saving source page %s", page_name)

        self.executor.exec_cmd("xdotool key 'ctrl+s'")
        wait_random_timeout(0.2, 0.5)

        cmd = "xdotool type '{}'".format(page_name)
        self.executor.exec_cmd(cmd)
        wait_random_timeout(0.2, 0.5)

        self.executor.exec_cmd("xdotool key KP_Enter")
        wait_random_timeout(0.5, 0.6)
        self.executor.exec_cmd("xdotool key KP_Enter")
        wait_random_timeout(1, 2)
