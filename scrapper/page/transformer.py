from abc import abstractmethod
from os import path


def get_page_transformer(log, settings):
    return BatchPageTransformer(log, settings)


class AbstractPageTransformer(object):

    def __init__(self, log, settings):
        self.log = log
        self.settings = settings

    @abstractmethod
    def transform_current_page(self, web_driver):
        pass


class BatchPageTransformer(AbstractPageTransformer):
    def transform_current_page(self, web_driver):

        page_name = web_driver.get_page_name()
        self.log.info("Transforming page %s", page_name)

        try:
            script = self.get_transformation_script()
            web_driver.execute_script(script)
        except:
            self.log.exception("Error transform page %s", page_name)

    def get_transformation_script(self):

        f_path = path.join(self.settings.js_files_path, "transform.js")

        with open(f_path, "r") as f:
            return f.read()
