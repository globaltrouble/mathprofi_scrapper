import os
import getpass
from traceback import format_exc as tb

from scrapper.constants import seleniumDriverType

ROOT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CONFIG_FILE_PATH = os.path.join(ROOT_PATH, "settings.ini")


def get_settings():
    return Settings.from_file(CONFIG_FILE_PATH)


class Settings(object):

    root_path = ROOT_PATH
    store_path = os.path.join(ROOT_PATH, "store")

    log_path = os.path.join(root_path, "logs")
    log_name = "scrapper.log"

    js_files_path = os.path.join(ROOT_PATH, "js")

    tests_path = os.path.join(ROOT_PATH, "tests")
    tests_files_path = os.path.join(tests_path, "files")

    selenium_driver_type = seleniumDriverType.CHROME

    save_path = "/home/{}/Downloads/".format(getpass.getuser())

    start_page = "http://mathprofi.ru"

    def __init__(self):
        self.check_settings()

    def check_settings(self):
        attrs = list(self.__class__.__dict__.items()) + list(self.__dict__.items())
        for key, val in attrs:
            if "path" in key and key != "check_path":
                try:
                    self.check_path(key, val)
                except:
                    print(key, tb())

    def check_path(self, key, val):
        try:
            if not os.path.exists(val):
                print("Creating path: ", val, " for key: ", key)
                os.mkdir(val)
        except:
            print("Error craete path: ", val, ", key: ", key, ", tb: ", tb())

    @classmethod
    def from_file(cls, file_path):
        # TODO: add config file!
        return cls()