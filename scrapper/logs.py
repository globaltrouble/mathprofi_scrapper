import logging
from os import path

from .settings import get_settings

settings = get_settings()


def get_logger(name, log_level=logging.DEBUG, fmt="%(asctime)7s PID: %(process)7s %(levelname)7s: %(message)s"):

    formatter = logging.Formatter(fmt, datefmt="%d/%m/%Y %I:%M:%S")
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)

    log_file = path.join(settings.log_path, name)
    file_handler = logging.FileHandler(log_file, mode="a+")
    file_handler.setFormatter(formatter)

    log = logging.getLogger(name)
    log.addHandler(file_handler)
    log.addHandler(stream_handler)
    log.setLevel(log_level)

    return log
