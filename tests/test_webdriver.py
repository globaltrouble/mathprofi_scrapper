import time
from .utils_for_tests import get_test_log_and_settings
from scrapper.webDriver import get_driver


def test_webdriver():
    log, settings = get_test_log_and_settings()

    target_url = "http://mathprofi.ru/vektory_dlya_chainikov.html"
    expected = "vektory_dlya_chainikov"

    with get_driver(log, settings) as driver:
        driver.get(target_url)
        time.sleep(1)
        page_name = driver.get_page_name()
        assert page_name == expected, "`{}` != `{}`".format(page_name, expected)