from getpass import getuser
from scrapper.cmdExecutor import get_executor
from .utils_for_tests import get_test_log_and_settings, assert_equal


def test_executor():
    log, settings = get_test_log_and_settings()
    executor = get_executor(log, settings)

    cmd_res = executor.exec_cmd("whoami")

    actual = cmd_res.stdout[:-1].decode()
    assert_equal(actual, getuser())