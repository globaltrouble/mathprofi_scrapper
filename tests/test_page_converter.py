import os
from scrapper.page.converter import get_page_converter
from .utils_for_tests import get_test_log_and_settings


def test_page_converter():
    log, settings = get_test_log_and_settings()

    converter = get_page_converter(log, settings)

    src_path = "https://www.google.com"
    dst_folder = settings.tests_files_path

    target_name = converter.get_target_name(src_path)
    target_path = os.path.join(dst_folder, target_name)

    assert target_path.endswith(converter.get_file_extension())



    converter.convert_page(src_path, dst_folder)

    assert os.path.exists(target_path)
    assert os.path.getsize(target_path) > 10