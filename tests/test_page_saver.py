import os
import time
from scrapper.webDriver import get_driver
from scrapper.page.saver import get_page_saver
from .utils_for_tests import get_test_log_and_settings, assert_equal, remove_if_exists


def test_page_saver():
    log, settings = get_test_log_and_settings()

    saver = get_page_saver(log, settings)
    target_url = "http://mathprofi.ru/vektory_dlya_chainikov.html"
    save_dir = saver.get_save_path()

    with get_driver(log, settings) as driver:
        driver.get(target_url)
        time.sleep(1)
        page_name = driver.get_page_name()

        assert_equal(page_name, "vektory_dlya_chainikov")

        target_file = os.path.join(save_dir, page_name + ".html")
        remove_if_exists(target_file)

        saver.save_current_page(page_name, driver)

    assert os.path.exists(target_file)
    assert os.path.getsize(target_file) > 10