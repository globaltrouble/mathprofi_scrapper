import os
import time
from scrapper.logs import get_logger
from scrapper.settings import get_settings


def get_test_log_and_settings():
    log = get_logger("tests")
    settings = get_settings()

    return log, settings


def assert_equal(actual, expected):
    assert actual == expected, "`{}` != `{}`".format(actual, expected)


def remove_if_exists(target_path):
    if os.path.exists(target_path):
        os.remove(target_path)
        time.sleep(1)