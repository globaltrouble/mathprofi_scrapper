from scrapper.page.locator import get_page_locator
from scrapper.webDriver import get_driver
from .utils_for_tests import get_test_log_and_settings


def test_page_locator():
    log, settings = get_test_log_and_settings()

    locator = get_page_locator(log, settings)
    with get_driver(log, settings) as driver:
        links = locator.get_target_links(driver)

    assert len(links) > 170

